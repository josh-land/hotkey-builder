from pynput.keyboard import Key, KeyCode, Listener, Controller
import function_press_y

# TODO: trigger hotkey multiple times by holding shift and pressing a, a

PauseListener = 0
kbc = Controller()
MODIFIER_VKS = (160, 162, 91, 164, 165, 163, 161)

def run_hotkey(hotkey_func, *args):
    pressed_modifiers = [vk for vk in pressed_vks if vk in MODIFIER_VKS]
    
    kbc.press(KeyCode(vk=999)) # get_vk returns None. Used to pause listener
    for mod_vk in pressed_modifiers:
        kbc.release(KeyCode(vk=mod_vk))

    hotkey_func(*args)
                    
    for mod_vk in pressed_modifiers:
        kbc.press(KeyCode(vk=mod_vk))
    kbc.release(KeyCode(vk=999))

def function_1():
    """ One of your functions to be executed by a combination """
    run_hotkey(function_press_y.press_y, kbc)
    

def function_2():
    """ Another one of your functions to be executed by a combination """
    print('Executed function_2')


# Create a mapping of keys to function (use frozenset as sets/lists are not hashable - so they can't be used as keys)
# Note the missing `()` after function_1 and function_2 as want to pass the function, not the return value of the function
combination_to_function = {
    frozenset([Key.shift, KeyCode(vk=65)]): function_1,  # shift + a
    frozenset([Key.shift, KeyCode(vk=66)]): function_2,  # shift + b
    frozenset([Key.alt_l, KeyCode(vk=71)]): function_2,  # left alt + g
}


# The currently pressed keys (initially empty)
pressed_vks = set()


def get_vk(key):
    """
    Get the virtual key code from a key.
    These are used so case/shift modifications are ignored.
    """
    return key.vk if hasattr(key, 'vk') else key.value.vk


def is_combination_pressed(combination):
    """ Check if a combination is satisfied using the keys pressed in pressed_vks """
    # Exact length of pressed keys to trigger: alt+a+b won't trigger alt+a
    if len(pressed_vks) != len(combination):
        return False
    else:
        return all([get_vk(key) in pressed_vks for key in combination])

def on_press(key):
    """ When a key is pressed """
    if None in pressed_vks: return # Pause listener workaround
    vk = get_vk(key)  # Get the key's vk
    pressed_vks.add(vk)  # Add it to the set of currently pressed keys
    
    for combination in combination_to_function:  # Loop through each combination
        if is_combination_pressed(combination):  # Check if all keys in the combination are pressed
            combination_to_function[combination]()  # If so, execute the function

def on_release(key):
    """ When a key is released """
    if len(pressed_vks) == 0:
        return
    vk = get_vk(key)  # Get the key's vk
    if vk in pressed_vks: pressed_vks.remove(vk)  # Remove it from the set of currently pressed keys
 

with Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
